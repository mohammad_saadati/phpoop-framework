<?php
    define("APPROOT", dirname(dirname(__FILE__)));
    define("URLROOT", "URL");

    // Database 
    define("DB_DRIVER",   "DRIVER"                             );
    define("DB_HOST",     "HOST"                         );
    define("DB_NAME",     "DB_NAME"                            );
    define("DB_USER",     "DB_USER"                              );
    define("DB_PASSWORD", "DB_PASS"                                  );
?>