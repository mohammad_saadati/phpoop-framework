<?php
    class Posts extends Controller
    {
        private $post;

        public function __construct() 
        {
        }
        public function posts($id)
        {
            $data = [
                "id"    => $id,
            ];

            $this->views('Post/index', $data);
        }
    }
?>