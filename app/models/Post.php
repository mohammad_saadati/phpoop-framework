<?php
    class Post 
    {
        private $dbConn;

        public function __construct()
        {
            $this->dbConn = new Database;
        }
        public function getPosts()
        {
            $this->dbConn->query("select * from posts");
            return $this->dbConn->resultSet();
        }
    }
?>