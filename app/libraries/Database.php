<?php
    class Database 
    {
        private $dbh;
        private $error;
        private $stmt;

        public function __construct() 
        {
            $this->createConnection(); 
        }
        public function createConnection() {
            $dsn     = DB_DRIVER.":host=".DB_HOST.";dbname=".DB_NAME;
            $user    = DB_USER;
            $pass    = DB_PASSWORD;
            $options = [
                        PDO::ATTR_PERSISTENT => true,
                        PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION
                    ];
            try 
            {
                $this->dbh = new PDO($dsn, $user, $pass, $options);
            }
            catch(PDOException $e)
            {
                $this->error = $e->getMessage();
                echo $this->error;
            }
        }
        public function query($sql) 
        {
            $this->stmt = $this->dbh->prepare($sql);
        }
        public function bind($param, $value, $type = null)
        {
            if(is_null($type))
            {
                switch(gettype($value))
                {
                    case "NULL":
                        $this->stmt->bindValue($param, $value, PDO::PARAM_NULL);
                        break;
                    case "integer":
                        $this->stmt->bindValue($param, $value, PDO::PARAM_INT);
                        break;
                    case "boolean":
                        $this->stmt->bindValue($param, $value, PDO::PARAM_BOOL);
                        break;
                    default:
                        $this->stmt->bindValue($param, $value, PDO::PARAM_STR);
                }
                return;
            }

            $this->stmt->bindValue($param, $value, $type);
        }
        public function execute()
        {
            return $this->stmt->execute();
        }
        public function resultSet()
        {
            $this->execute();
            return $this->stmt->fetchAll(PDO::FETCH_OBJ);
        }
        public function result()
        {
            $this->execute();
            return $this->stmt->fetch(PDO::FETCH_OBJ);
        }
        public function rowCount()
        {
            return $this->stmt->rowCount();
        }
    }
?>