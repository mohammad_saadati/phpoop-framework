<?php
    class Core 
    {
        private $currentController = "Page";
        private $currentMethod = "index";
        private $params = [];
        private $url;

        public function __construct()
        {
            $this->url = $this->getURL();
            $this->checkControllerExists();
            $this->importController();
            $this->currentController = new $this->currentController();
            $this->checkControllerMethod();
            $this->callControllerMethod();
        }
        
        public function getURL()
        {
            if(isset($_GET['url']))
            {
                $url = rtrim($_GET['url']);
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);
                return $url;
            }
        }
        public function checkControllerExists()
        {
            if(file_exists("../app/controllers/" . ucwords($this->url[0]) . ".php"))
            {
                $this->currentController = ucwords($this->url[0]);
                unset($this->url[0]);
            }
        }
        public function importController()
        {
            include_once "../app/controllers/" . $this->currentController . ".php";
        }
        public function checkControllerMethod()
        {
            if(method_exists($this->currentController, $this->url[1]))
            {
                $this->currentMethod = $this->url[1];
                unset($this->url[1]);
            }
        }
        public function callControllerMethod() 
        {
            $this->params = $this->url ? array_values($this->url) : [];
            call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
        }
    }
?>